# Idle Pets

## Prerequisites

For the back-end stuff you'll need:
- Python 3.7
- Virtualenv

For the front-end stuff you'll need:
- Node.js
- Yarn
- Vue CLI 3

## Setup

1. First of all, pull the repo.

2. In the root folder, open a terminal window and make a virtual environment named "venv".

```
virtualenv venv
```

3. Go into the environment.

```
MacOS and Linux:
source venv/bin/activate

Windows:
.\venv\Scripts\activate
```

4. You're going to have to install a MySQL dependency depending on your OS:

Linux | Windows
--- | ---
Simply run ```sudo apt-get install libmysqlclient-dev``` | If you're on Windows, then [go to this link](https://www.lfd.uci.edu/~gohlke/pythonlibs/#mysqlclient) and download the wheel file that corresponds to your Python version and architecture (e.g. if you're using 32-bit Python 3.7, then download the one titled `mysqlclient‑1.4.6‑cp37‑cp37m‑win32.whl`). If you don't know your Python version, use `python --version` and if you don't know your Python architecture, run `python -c "import struct;print( 8 * struct.calcsize('P'))"`. Once you've downloaded that, place that in the root folder and run `pip install [name of .whl file]`. Once you've installed that, go into `requirements.txt` and remove the line with `mysqlclient` in it. We delete this because when we try the next step, pip will try to install `mysqlclient` when we already have it installed and it'll fail, so be sure to remove that line! (Just don't commit it, so that Linux users will be ok too)


5. Install all the dependencies this project has.

```
pip install -r requirements.txt
```

6. If you removed the `mysqlclient` line back in step 4, put it back in. Or just discard your changes through Git.

## Running locally

Make sure you're in the virtual environment!

1. Migrate everything (if you've already done this before and want to "refresh", just delete the `migrations` folder in `/backend/api` and `db.sqlite3` file in the root).

```
python manage.py makemigrations api
python manage.py migrate
```

2. Install front-end dependencies.

```
yarn install
```

3. Build front-end.

```
yarn build
```

4. Run server.

```
python manage.py runserver
```

## Deploying to GAE

Merge your branch to master.

This repo has continuous deployment, so every time something is pushed to master, it'll automatically be deployed. So don't worry!

It will deploy to https://idlepets.appspot.com/

## Template Structure


| Location             |  Content                                   |
|----------------------|--------------------------------------------|
| `/backend`           | Django Project & Backend Config            |
| `/backend/api`       | Django App (`/api`)                        |
| `/src`               | Vue App .                                  |
| `/src/main.js`       | JS Application Entry Point                 |
| `/public/index.html` | Html Application Entry Point (`/`)         |
| `/public/static`     | Static Assets                              |
| `/dist/`             | Bundled Assets Output (generated at `yarn build`) |