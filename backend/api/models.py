from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from rest_framework import serializers


class Item(models.Model):
    ITEM_TYPE = (
        ("H", "HAT"),
        ("F", "FACE"),
        ("T", "TOY")
    )
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    type = models.CharField(max_length=1, choices=ITEM_TYPE)
    rarity = models.CharField(max_length=255)
    idle = models.FloatField()
    multiplier = models.FloatField()
    click = models.FloatField()
    owner = models.ForeignKey("User", on_delete=models.CASCADE)
    sprite = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class UserManager(BaseUserManager):
    use_in_migrations = True

    def create_user(self, username, password=None, **kwargs):
        user = self.model(
            username=username,
            **kwargs
        )
        user.set_password(password)
        user.save(using=self._db)

        # Creates default items
        default_hat = Item.objects.create(name="Rookie's Hat", description="It's almost useless...", type="H", rarity="Common", idle="0", multiplier="0", click="1", owner=user, sprite="Nothing")
        default_face = Item.objects.create(name="Rookie's Accessory", description="Better find something better...", type="F", rarity="Common", idle="0", multiplier="1", click="0", owner=user, sprite="Nothing")
        default_toy = Item.objects.create(name="Rookie's Toy", description="It's no fun at all...", type="T", rarity="Common", idle="0", multiplier="0", click="0", owner=user, sprite="Nothing")
        
        # Updates the equipped items of the user
        user.equipped_items_hat = default_hat
        user.equipped_items_face = default_face
        user.equipped_items_toy = default_toy
        user.save(using=self._db)

        return user

class User(AbstractBaseUser):
    username = models.CharField(max_length=255, primary_key=True)
    ap = models.BigIntegerField(default=0)
    lifetime_ap = models.BigIntegerField(default=0)
    hunger = models.FloatField(default=0.5)
    equipped_items_hat = models.ForeignKey(Item,
                                           on_delete=models.SET_NULL,
                                           related_name="hat",
                                           blank=True, null=True,
                                           limit_choices_to={"type": "H"})
    equipped_items_face = models.ForeignKey(Item, on_delete=models.SET_NULL,
                                            related_name="face",
                                            blank=True, null=True,
                                            limit_choices_to={"type": "F"})
    equipped_items_toy = models.ForeignKey(Item, on_delete=models.SET_NULL,
                                           related_name="toy",
                                           blank=True, null=True,
                                           limit_choices_to={"type": "T"})

    USERNAME_FIELD = "username"
    objects = UserManager()

    def __str__(self):
        return self.username

    def inventory(self):
        """
        Gets this user's inventory
        :return: a QuerySet of the inventory of this user
        """
        return Item.objects.filter(owner__username=self.username)

class Followers(models.Model):
    follower = models.ForeignKey(User, 
                                 on_delete=models.CASCADE, 
                                 related_name="follower")
    following = models.ForeignKey(User,
                                  on_delete=models.CASCADE,
                                  related_name="following")

    def __str__(self):
        return "{} -> {}".format(str(self.follower), str(self.following))

    class Meta:
        unique_together = (("follower", "following"))

class Email(models.Model):
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name="sender")
    recipent = models.ForeignKey(User, on_delete=models.CASCADE, related_name="recipent")
    subject = models.CharField(max_length=255)
    content = models.CharField(max_length=255)

    def __str__(self):
        return f"{str(sender.username)} -> {str(recepient.username)}"


class Message(models.Model):
    subject = models.CharField(max_length=200)
    body = models.TextField()
