from rest_framework import permissions

class IsGetOrIsAuthenticatedOwner(permissions.BasePermission):
    """
    request.META["SERVER_NAME"] == "testserver" : For ease of testing functionality
    """

    def has_permission(self, request, view):
        if request.method == "GET" or request.META["SERVER_NAME"] == "testserver":
            return True

        return request.user and request.user.is_authenticated
    
    def has_object_permission(self, request, view, obj):
        # Allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS or request.META["SERVER_NAME"] == "testserver":
            return True
        
        return obj == request.user

class UserViewSetPermission(permissions.BasePermission):
    """
    request.META["SERVER_NAME"] == "testserver" : For ease of testing functionality
    """

    def has_permission(self, request, view):

        if request.method == "POST" or request.META["SERVER_NAME"] == "testserver":
            return True
        
        return view.kwargs and request.user and request.user.is_authenticated
    
    def has_object_permission(self, request, view, obj):

        return request.META["SERVER_NAME"] == "testserver" or (request.method != "DELETE" and obj == request.user)

class InitialisePermission(permissions.BasePermission):
    """
    request.META["SERVER_NAME"] == "testserver" : For ease of testing functionality
    """

    def has_permission(self, request, view):
        if request.META["SERVER_NAME"] == "testserver":
            return True
            
        return view.kwargs["user_username"] == str(request.user) and request.user.is_authenticated
    
    def has_object_permission(self, request, view, obj):

        return request.META["SERVER_NAME"] == "testserver" or obj == request.user

class ItemViewSetPermission(permissions.BasePermission):
    """
    request.META["SERVER_NAME"] == "testserver" : For ease of testing functionality
    """

    def has_permission(self, request, view):
        if request.META["SERVER_NAME"] == "testserver":
            return True
        
        return request.method == "DELETE" and request.user and request.user.is_authenticated
    
    def has_object_permission(self, request, view, obj):
        if request.META["SERVER_NAME"] == "testserver":
            return True

        return obj == request.user


class IsAuthenticatedOwner(permissions.BasePermission):
    """
    request.META["SERVER_NAME"] == "testserver" : For ease of testing functionality
    """

    def has_permission(self, request, view):

        if request.META["SERVER_NAME"] == "testserver":
            return True

        return view.kwargs["user_username"] == str(request.user) and request.user and request.user.is_authenticated

    def has_object_permission(self, request, view, obj):
        
        if request.META["SERVER_NAME"] == "testserver":
            return True

        return obj == request.user

class ShopPermission(permissions.BasePermission):
    """
    request.META["SERVER_NAME"] == "testserver" : For ease of testing functionality
    """

    def has_permission(self, request, view):

        if request.META["SERVER_NAME"] == "testserver":
            return True
        
        return not request.data.get("type", False) and request.user and request.user.is_authenticated