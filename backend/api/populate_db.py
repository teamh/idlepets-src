from backend.api.models import User, Item
from datetime import datetime
from django.utils.timezone import make_aware

user = User.objects.create_user(username="testing1234", password="testing1234")
hat = Item.objects.create(name="Hat", description="This is a hat", type="HAT", rarity="Rare", idle="10", multiplier="2", click="20", owner=user, sprite="null")
face = Item.objects.create(name="Face", description="This is a face", type="FACE", rarity="Common", idle="1", multiplier="1", click="5", owner=user, sprite="null")
toy = Item.objects.create(name="Toy", description="This is a toy", type="TOY", rarity="Epic", idle="4", multiplier="3", click="8", owner=user, sprite="null")
user.equipped_items_hat = hat
user.equipped_items_face = face
user.equipped_items_toy = toy
user.last_login = make_aware(datetime.now()
user.save()
