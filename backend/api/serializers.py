from rest_framework import serializers
from django.core.exceptions import ObjectDoesNotExist

from backend.api.models import Item, Message, User, Followers, Email


class ItemSerialiser(serializers.HyperlinkedModelSerializer):
    owner = serializers.HyperlinkedRelatedField(view_name="user-detail", lookup_field="username", queryset=User.objects.all())

    class Meta:
        model = Item
        fields = ("id","name", "description", "type", "rarity", "idle", "multiplier", "click", "owner", "sprite")


class UserSerialiser(serializers.HyperlinkedModelSerializer):
    # equipped_items_hat = serializers.HyperlinkedIdentityField(many=False, view_name="item-list", read_only=False, format='html')
    # equipped_items_face = serializers.ReadOnlyField(source='equipped_items_face.name')
    # equipped_items_toy = serializers.ReadOnlyField(source='equipped_items_toy.name')

    def create(self, validated_data):
        u, p = validated_data["username"], validated_data["password"]
        return User.objects.create_user(u,p)

    class Meta:
        model = User
        fields = ("username", "password", "ap", "lifetime_ap", "hunger", "equipped_items_hat", "equipped_items_face",
                  "equipped_items_toy", "last_login")

class FollowersSerializer(serializers.ModelSerializer):
    # follower = serializers.HyperlinkedRelatedField(view_name="user-detail", lookup_field="username", queryset=User.objects.all())
    # following = serializers.HyperlinkedRelatedField(view_name="user-detail", lookup_field="username", queryset=User.objects.all())

    class Meta:
        model = Followers
        fields = ("follower", "following")

class EmailSerializer(serializers.ModelSerializer):
    # from = serializers.HyperlinkedRelatedField(view_name="user-detail", lookup_field="username", queryset=User.objects.all())
    # to = serializers.HyperlinkedRelatedField(view_name="user-detail", lookup_field="username", queryset=User.objects.all())

    class Meta:
        model = Email
        fields = ("id", "sender", "recipent", "subject", "content")

class MessageSerializer(serializers.HyperlinkedModelSerializer):
    
    class Meta:
        model = Message
        fields = ('url', 'subject', 'body', 'pk')
