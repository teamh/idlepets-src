from django.test import TestCase
from rest_framework.test import APIRequestFactory, APIClient, RequestsClient

from backend.api.models import User, Item, Followers, Email
from backend.api.serializers import ItemSerialiser

from django.utils.timezone import make_aware
from datetime import datetime
import time
from math import ceil, floor, log

api_host = "http://testserver/api/"


class UserTests(TestCase):

    def setUp(self) -> None:
        self.owner = User.objects.create_user(username="user1", password="pass1", ap=1, lifetime_ap=2, hunger=3)
        self.item1 = Item.objects.create(name="hat", description="hat", type="H", rarity="rare", idle=1, multiplier=2,
                                         click=3, owner=self.owner, sprite="sprite.com")
        self.item2 = Item.objects.create(name="face", description="face", type="F", rarity="rare", idle=1, multiplier=2,
                                         click=3, owner=self.owner, sprite="sprite.com")
        self.item3 = Item.objects.create(name="toy", description="toy", type="T", rarity="rare", idle=1, multiplier=2,
                                         click=3, owner=self.owner, sprite="sprite.com")
        self.item4 = Item.objects.create(name="misc", description="toy", type="T", rarity="rare", idle=1, multiplier=2,
                                         click=3, owner=self.owner, sprite="sprite.com")

    def test_inventory(self):
        # Gets inventory
        inventory = self.owner.inventory()

        # Checks if all the items are contained
        inventory = list(map(lambda x: x.name, list(inventory)))
        self.assertIn("hat", inventory)
        self.assertIn("face", inventory)
        self.assertIn("toy", inventory)
        self.assertIn("misc", inventory)

class AuthenticationTests(TestCase):

    def setUp(self) -> None:
        User.objects.create_user(username="testing123", password="testing123")
        self.client = RequestsClient()

    def test_valid_login(self):
        # Send a login request with valid credentials
        response = self.client.post(api_host + "token/", json={
            "username": "testing123",
            "password": "testing123"
        })
        
        # Check if logged in successfully
        self.assertEqual(response.status_code, 200)

        # Check if access and refresh tokens are recevied
        tokens = response.json()
        self.assertTrue(len(tokens) == 2)
        self.assertTrue(tokens["access"])
        self.assertTrue(tokens["refresh"])

        # Send a login request with a valid access token
        response = self.client.post(api_host + "token/verify/", json={
            "token" : tokens["access"]
        })
        self.assertEqual(response.status_code, 200)

        # Send a request to get a new access token with a valid refresh token
        response = self.client.post(api_host + "token/refresh/", json={
            "refresh" : tokens["refresh"]
        })

        self.assertEqual(response.status_code, 200)
        self.assertTrue(len(response.json()) == 1)
        self.assertTrue(tokens["access"])

    def test_invalid_login(self):
        response = self.client.post(api_host + "token/", json={
            "username": "testing123",
            "password": "wrongpassword"
        })

        self.assertEqual(response.status_code, 401)

class InitialiseAndSaveOnLogoutTests(TestCase):
    def setUp(self) -> None:
        self.user1 = User.objects.create_user(username="user1", password="pass1", ap=100, lifetime_ap=600, hunger=1, last_login=make_aware(datetime.now()))
        self.default_hat = Item.objects.create(name="Rookie hat", description="Hat?", type="H", rarity="Common", idle="1", multiplier="1", click="5", owner=self.user1, sprite="null")
        self.default_face = Item.objects.create(name="Rookie face", description="Face?", type="F", rarity="Common", idle="1", multiplier="1", click="5", owner=self.user1, sprite="null")
        self.default_toy = Item.objects.create(name="Rookie toy", description="Toy?", type="T", rarity="Common", idle="1", multiplier="1", click="5", owner=self.user1, sprite="null")
        
        # Updates the equipped items of the user
        self.user1.equipped_items_hat = self.default_hat
        self.user1.equipped_items_face = self.default_face
        self.user1.equipped_items_toy = self.default_toy
        self.user1.save()

        self.client = RequestsClient()
    
    def test_inititialise_user_data(self):
        """
        Test initialising user data when user logs in
        """
        # time.sleep(5)
        response = self.client.get(api_host + "user/user1/initialise/")
        res_body = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(res_body), 5)
        # self.assertEqual(res_body["ap"], 5 * 0.5 * 3 * 3)
        # self.assertEqual(res_body["hunger"], 1 - 5 * (1 / (30 * 60)))
    
    def test_save_on_logout(self):
        """
        Test saving the current state of the user when logs out
        """
        new_ap, new_hunger, ap_gain = 240, 0.55, 150

        response = self.client.patch(api_host + "user/user1/", json={
            "ap" : new_ap,
            "hunger" : new_hunger,
            "ap_gain" : 150
        })
        self.assertEqual(response.status_code, 200)

        updated_user1 = User.objects.get(username="user1")
        self.assertEqual(updated_user1.ap, new_ap)
        self.assertEqual(updated_user1.hunger, new_hunger)
        self.assertEqual(updated_user1.lifetime_ap, self.user1.lifetime_ap + ap_gain)

class GeneratingItemTests(TestCase):
    def setUp(self) -> None:
        self.user1 = User.objects.create_user(username="user1", password="pass1", ap=693, lifetime_ap=951, hunger=3)

        self.client = RequestsClient()
    
    def test_generate_new_common_item(self):
        """
        Test generating a random common item
        """
        types = ["H", "T", "F"]
        typesMapStats = {
            "H" : [floor(self.user1.lifetime_ap * 0.1 / 100) + 1, ceil(self.user1.lifetime_ap * 1 / 100) + 1],
            "T" : [floor(self.user1.lifetime_ap * 0.1 / 100) + 1, ceil(self.user1.lifetime_ap * 1 / 100) + 1],
            "F" : [max(ceil(100/100 * log(self.user1.lifetime_ap, 64)), 1), max(ceil(200/100 * log(self.user1.lifetime_ap, 64)), 1)]
        }

        for type in types:
            response = self.client.post(api_host + "shop/", json={
                "username" : "user1",
                "rarity" : "Common",
                "type" : type
            })
            self.assertEqual(response.status_code, 200)
            res_body = response.json()
            
            self.assertEqual(len(res_body), 10)
            
            if type == "H":
                self.assertTrue(typesMapStats[type][0] <= res_body["click"] <= typesMapStats[type][1])
                self.assertEqual(res_body["idle"], 0)
                self.assertEqual(res_body["multiplier"], 0)
            elif type == "T":
                self.assertTrue(typesMapStats[type][0] <= res_body["idle"] <= typesMapStats[type][1])
                self.assertEqual(res_body["click"], 0)
                self.assertEqual(res_body["multiplier"], 0)
            else:
                self.assertTrue(typesMapStats[type][0] <= res_body["multiplier"] <= typesMapStats[type][1])
                self.assertEqual(res_body["idle"], 0)
                self.assertEqual(res_body["click"], 0)

    
    def test_generate_new_rare_item(self):
        """
        Test generating a random rare item
        """
        types = ["H", "T", "F"]
        typesMapStats = {
            "H" : 
            {
                "click": [floor(self.user1.lifetime_ap * 0.4 / 100) + 1, ceil(self.user1.lifetime_ap * 1.4 / 100) + 1],
                "idle": [floor(self.user1.lifetime_ap * 0.2 / 100) + 1, ceil(self.user1.lifetime_ap * 1.2 / 100) + 1]
            },
            "T" : 
            {
                "click": [floor(self.user1.lifetime_ap * 0.4 / 100) + 1, ceil(self.user1.lifetime_ap * 1.4 / 100) + 1],
                "idle": [floor(self.user1.lifetime_ap * 0.2 / 100) + 1, ceil(self.user1.lifetime_ap * 1.2 / 100) + 1]
            },
            "F" : 
            {
                "click": [floor(self.user1.lifetime_ap * 0.4 / 100) + 1, ceil(self.user1.lifetime_ap * 1.4 / 100) + 1],
                "multiplier": [max(ceil(130/100 * log(self.user1.lifetime_ap, 64)), 1), max(ceil(230/100 * log(self.user1.lifetime_ap, 64)), 1)]
            }
        }

        for type in types:
            response = self.client.post(api_host + "shop/", json={
                "username" : "user1",
                "rarity" : "Rare",
                "type" : type
            })
            self.assertEqual(response.status_code, 200)
            res_body = response.json()
            
            self.assertEqual(len(res_body), 10)
            if type == "H" or type == "T":
                self.assertTrue(typesMapStats[type]["click"][0] <= res_body["click"] <= typesMapStats[type]["click"][1])
                self.assertTrue(typesMapStats[type]["idle"][0] <= res_body["idle"] <= typesMapStats[type]["idle"][1])
                self.assertEqual(res_body["multiplier"], 0)
            else:
                self.assertTrue(typesMapStats[type]["click"][0] <= res_body["click"] <= typesMapStats[type]["click"][1])
                self.assertTrue(typesMapStats[type]["multiplier"][0] <= res_body["multiplier"] <= typesMapStats[type]["multiplier"][1])
                self.assertEqual(res_body["idle"], 0)

    def test_generate_new_epic_item(self):
        """
        Test generating a random epic item
        """
        types = ["H", "T", "F"]
        statsMap = {
            "click": [floor(self.user1.lifetime_ap * 0.8 / 100) + 1, ceil(self.user1.lifetime_ap * 1.8 / 100) + 1],
            "idle": [floor(self.user1.lifetime_ap * 0.6 / 100) + 1, ceil(self.user1.lifetime_ap * 1.6 / 100) + 1],
            "multiplier": [max(ceil(160/100 * log(self.user1.lifetime_ap, 64)), 1), max(ceil(260/100 * log(self.user1.lifetime_ap, 64)), 1)]
        }
        
        for type in types:
            response = self.client.post(api_host + "shop/", json={
                "username" : "user1",
                "rarity" : "Epic",
                "type" : type
            })
            self.assertEqual(response.status_code, 200)
            res_body = response.json()
            
            self.assertEqual(len(res_body), 10)

            self.assertTrue(statsMap["click"][0] <= res_body["click"] <= statsMap["click"][1])
            self.assertTrue(statsMap["multiplier"][0] <= res_body["multiplier"] <= statsMap["multiplier"][1])
            self.assertTrue(statsMap["idle"][0] <= res_body["idle"] <= statsMap["idle"][1])

class FollowersTests(TestCase):
    def setUp(self) -> None:
        self.user1 = User.objects.create_user(username="user1", password="pass1", ap=1, lifetime_ap=2, hunger=3)
        self.user2 = User.objects.create_user(username="user2", password="pass3", ap=4, lifetime_ap=1, hunger=2)
        self.user3 = User.objects.create_user(username="user3", password="pass5", ap=4, lifetime_ap=1, hunger=2)

        # Establish relations between users
        Followers.objects.create(follower=self.user1, following=self.user2)
        Followers.objects.create(follower=self.user2, following=self.user3)
        Followers.objects.create(follower=self.user1, following=self.user3)

        self.client = RequestsClient()

    def test_followings_list(self):
        """
        Test getting a list of user's following
        """
        response = self.client.get(api_host + "user/user1/following/")
        res_body = response.json()
        
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(res_body["followings"]), 2)

    def test_followers_list(self):
        """
        Test getting a list of user's followers
        """
        response = self.client.get(api_host + "user/user3/followers/")
        res_body = response.json()
        
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(res_body["followers"]), 2)

    def test_follow_user(self):
        """
        Test following a user
        """
        response = self.client.post(api_host + "user/user3/following/", json={
            "following": "user2",
        })
        self.assertEqual(response.status_code, 201)

        response = self.client.get(api_host + "user/user3/following/")
        res_body = response.json()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(res_body["followings"]), 1)
    
    def test_unfollow_user(self):
        """
        Test unfollowing a user
        """
        response = self.client.delete(api_host + "user/user2/following/user3/")
        self.assertEqual(response.status_code, 204)

        response = self.client.get(api_host + "user/user2/following/")
        res_body = response.json()
        self.assertEqual(len(res_body["followings"]), 0)

    def test_unfollow_everyone(self):
        """
        Test unfollowing everyone
        """
        response = self.client.delete(api_host + "user/user1/following/")
        self.assertEqual(response.status_code, 204)
        
        response = self.client.get(api_host + "user/user1/following/")
        res_body = response.json()
        self.assertEqual(len(res_body["followings"]), 0)

class EmailTests(TestCase):

    def setUp(self) -> None:
        self.user1 = User.objects.create(username="user1", password="pass1", ap=1, lifetime_ap=2, hunger=3)
        self.user2 = User.objects.create(username="user2", password="pass3", ap=4, lifetime_ap=1, hunger=2)
        self.email1 = Email.objects.create(sender=self.user2, recipent=self.user1, subject="Testing1", content="Test1")
        self.email2 = Email.objects.create(sender=self.user2, recipent=self.user1, subject="Testing2", content="Test2")
        self.email3 = Email.objects.create(sender=self.user2, recipent=self.user1, subject="Testing3", content="Test3")

        self.client = RequestsClient()
    
    def retrieve_emails(self):
        """
        Test retrieving all the emails that are sent to user1
        """
        response = self.client.get(api_host + 'user/user1/email/')
        self.assertEqual(response.status_code, 200)
        
        res_body = response.json()
        self.assertEqual(len(res_body), 3)
        
        for i in range(len(res_body)):
            self.assertEqual(res_body[i]["sender"], "user2")
            self.assertEqual(res_body[i]["recipent"], "user1")
            self.assertEqual(res_body[i]["subject"], f"Testing{i+1}")
            self.assertEqual(res_body[i]["content"], f"Test{i+1}")

    def test_send_email(self):
        """
        Test sending emails to user2
        """
        response = self.client.post(api_host + 'user/user1/email/', json={
            "recipent" : "user2",
            "subject" : "Greetings",
            "content" : "Hello world"
        })
        self.assertEqual(response.status_code, 200)

        response = self.client.get(api_host + 'user/user2/email/')
        self.assertEqual(response.status_code, 200)

        res_body = response.json()
        self.assertEqual(len(res_body), 1)
        self.assertEqual(res_body[0]["sender"], "user1")
        self.assertEqual(res_body[0]["recipent"], "user2")
        self.assertEqual(res_body[0]["subject"], "Greetings")
        self.assertEqual(res_body[0]["content"], "Hello world")

    def test_delete_email(self):
        """
        Test removing an email from user1
        """
        response = self.client.delete(api_host + 'user/user1/email/', json={
            "id" : self.email1.id
        })
        self.assertEqual(response.status_code, 204)

        response = self.client.get(api_host + 'user/user1/email/')
        self.assertEqual(response.status_code, 200)

        res_body = response.json()
        self.assertEqual(len(res_body), 2)


class EndpointTests(TestCase):

    def setUp(self) -> None:
        self.user1 = User.objects.create(username="user1", password="pass1", ap=1, lifetime_ap=2, hunger=3)
        self.user2 = User.objects.create(username="user2", password="pass3", ap=4, lifetime_ap=1, hunger=2)
        self.item1 = Item.objects.create(name="hat", description="hat", type="H", rarity=1, idle=2, multiplier=3, click=4, owner=self.user1, sprite="sprite.com")
        self.item2 = Item.objects.create(name="mask", description="mask", type="F", rarity=1, idle=2, multiplier=3, click=4, owner=self.user1, sprite="sprite.com")
        self.item3 = Item.objects.create(name="toy", description="toy", type="T", rarity=1, idle=2, multiplier=3, click=4, owner=self.user1, sprite="sprite.com")
        self.item4 = Item.objects.create(name="alt_hat", description="alt_hat", type="H", rarity=1, idle=2, multiplier=3, click=4, owner=self.user2, sprite="sprite.com")

        # Equips items
        self.user1.equipped_items_hat = self.item1
        self.user1.equipped_items_face = self.item2
        self.user1.equipped_items_toy = self.item3

        self.factory = APIRequestFactory()
        self.client = RequestsClient()

    def test_get_user_list(self):
        """
        Tests fetching a list of users
        """
        response = self.client.get(api_host + "user/", params={
            "format": "json"
        })
        # There should exist some users
        self.assertTrue(len(response.json()) == 2)

    def test_create_user(self):
        """
        Test creating a new user
        """
        # Creates user
        response = self.client.post(api_host + "user/", json={
            "username": "new_username",
            "password": "new_password"
        })

        # Check if created successfully
        self.assertEqual(response.status_code, 201)

        # Check if a new user was added
        response = self.client.get(api_host + "user/new_username/", params={
            "format": "json"
        })
        self.assertEqual(response.status_code, 200)

    def test_get_user(self):
        """
        Test getting a user
        """
        # Tries to get user1
        response = self.client.get(api_host + "user/user1/", params={
            "format": "json"
        })
        self.assertEqual(response.status_code, 200)

    def test_update_user(self):
        """
        Test updating a user
        """
        # Trying to update user
        response = self.client.put(api_host + "user/user1/", json={
            "username": "user10",
            "password": "newpassword",
            "ap": 10
        })

        # Asserts status code
        self.assertEqual(response.status_code, 200)

        # Tests if we really did change the details
        response = self.client.get(api_host + "user/user10/", params={"format": "json"})
        user = response.json()
        self.assertEqual(user["password"], "newpassword")
        self.assertEqual(user["ap"], 10)

    def test_patch_user(self):
        """
        Test updating a part of the user
        """
        # Trying to patch user
        response = self.client.patch(api_host + "user/user1/", json={
            "hunger": 2.0,
        })
        self.assertEqual(response.status_code, 200)

        # Test if hunger has changed
        response = self.client.get(api_host + "user/user1/", params={"format": "json"})
        user = response.json()
        self.assertEqual(user["hunger"], 2.0)

    def test_delete_user(self):
        """
        Test deleting a user
        """
        # Delete user 1
        response = self.client.delete(api_host + "user/user1/")

        # Check if this user was deleted
        self.assertEqual(response.status_code, 204)

        # Try and find this user (we won't)
        response = self.client.get(api_host + "user/user1/")
        self.assertEqual(response.status_code, 404)

    def test_get_inventory_list(self):
        """
        Test getting a user's inventory
        """
        # Gets inventory
        response1 = self.client.get(api_host + "user/user1/inventory/", params={"format": "json"})
        response2 = self.client.get(api_host + "user/user2/inventory/", params={"format": "json"})

        # Check if the requests passed
        self.assertEqual(response1.status_code, 200)
        self.assertEqual(response2.status_code, 200)

        # Filter out names of items
        data1 = response1.json()
        item_names = map(lambda i: i["name"], data1)
        item_names = list(item_names)

        # Get inventory JSON of user 2
        data2 = response2.json()

        # Assert names
        self.assertIn("hat", item_names)
        self.assertIn("mask", item_names)
        self.assertIn("toy", item_names)

        # Assert that user 2 has only 1 item
        self.assertTrue(len(data2) == 1)

    def test_add_item_to_inventory(self):
        """
        Test putting an item into a user's inventory
        """
        response = self.client.put(api_host + "user/user2/inventory/", json={
            "id": self.item2.id
        })

        # Assert that the request passed
        self.assertEqual(response.status_code, 200)

        # Check that user 2 has item 2 (mask)
        user2_inventory = self.client.get(api_host + "user/user2/inventory/", params={"format": "json"}).json()
        user2_inventory_names = list(map(lambda i: i["name"], user2_inventory))
        self.assertIn("mask", user2_inventory_names)

        # Check that user 1 does not have item 2 (mask)
        user1_inventory = self.client.get(api_host + "user/user1/inventory/", params={"format": "json"}).json()
        user1_inventory_names = list(map(lambda i: i["name"], user1_inventory))
        self.assertNotIn("mask", user1_inventory_names)

    def test_delete_inventory(self):
        """
        Test deleting an entire inventory
        """
        response = self.client.delete(api_host + "user/user1/inventory/")

        # Assert that the request passed
        self.assertEqual(response.status_code, 204)

        # Assert that all items are gone (except for one)
        self.assertTrue(len(Item.objects.all()) == 1)

        # Assert that inventory is empty
        user1_inventory = self.client.get(api_host + "user/user1/inventory/", params={"format": "json"}).json()
        self.assertTrue(len(user1_inventory) == 0)

        # Assert that user 1 has no equipped items
        user1 = self.client.get(api_host + "user/user1/", params={"format": "json"}).json()
        self.assertIsNone(user1["equipped_items_hat"])
        self.assertIsNone(user1["equipped_items_face"])
        self.assertIsNone(user1["equipped_items_toy"])

    def test_get_inventory_item(self):
        """
        Test fetching an inventory item
        """
        response = self.client.get(api_host + f"user/user1/inventory/{self.item1.id}", params={"format": "json"})

        # Assert that the request passed
        self.assertEqual(response.status_code, 200)

        # Assert that this is the item we're looking for
        self.assertTrue(response.json()["name"] == self.item1.name)

    def test_get_inventory_item_we_dont_have(self):
        """
        Test fetching an inventory item we don't have (it should fail)
        """
        response = self.client.get(api_host + "user/user1/inventory/11037", params={"format": "json"})

        # Assert that we get nothing
        self.assertEqual(response.status_code, 404)

    def test_delete_inventory_item(self):
        """
        Test deleting an inventory item
        """
        response = self.client.delete(api_host + f"user/user1/inventory/{self.item1.id}")

        # Assert that the request passed
        self.assertEqual(response.status_code, 204)

        # Assert that the item no longer exists
        response = self.client.get(api_host + f"item/{self.item1.id}")
        self.assertEqual(response.status_code, 404)

        # Assert that the item is no longer in the inventory
        user1_inventory = self.client.get(api_host + "user/user1/inventory/", params={"format": "json"}).json()
        user1_inventory_names = list(map(lambda i: i["name"], user1_inventory))
        self.assertNotIn("hat", user1_inventory_names)

        # Assert that the item is no longer equipped
        user1 = self.client.get(api_host + "user/user1/", params={"format": "json"}).json()
        self.assertIsNone(user1["equipped_items_hat"])

    def test_equip_item(self):
        """
        Test equipping an item
        """
        response = self.client.get(api_host + f"user/user2/inventory/{self.item4.id}/equip", params={"format": "json"}).json()

        # Assert that user 2 is wearing the new item
        user2 = self.client.get(api_host + "user/user2/", params={"format": "json"}).json()
        self.assertIsNotNone(user2["equipped_items_hat"])

    def test_equip_item_we_dont_have(self):
        """
        Test equipping an item we don't have
        """
        response = self.client.get(api_host + f"user/user2/inventory/11037/equip", params={"format": "json"})

        # Assert that the request was invalid
        self.assertEqual(response.status_code, 404)

    def test_unequip_item(self):
        """
        Test unequipping an item
        """
        response = self.client.get(api_host + f"user/user1/inventory/{self.item1.id}/unequip", params={"format": "json"}).json()

        # Assert that user 1 is no longer wearing a hat
        user1 = self.client.get(api_host + "user/user1/", params={"format": "json"}).json()
        self.assertIsNone(user1["equipped_items_hat"])

    def test_unequip_item_we_dont_have(self):
        """
        Test unequipping an item we don't have
        """
        response = self.client.get(api_host + f"user/user2/inventory/11037/unequip", params={"format": "json"})

        # Assert that the request was invalid
        self.assertEqual(response.status_code, 404)

    def test_get_item_list(self):
        """
        Test getting all items
        """
        response = self.client.get(api_host + "item/", params={
            "format": "json"
        })
        # There should exist some users
        self.assertTrue(len(response.json()) == 4)

    def test_create_item(self):
        """
        Test creating an item
        """
        # Creates item
        response = self.client.post(api_host + "item/", json={
            "name": "new_item",
            "description": "desc",
            "type": "H",
            "rarity": 1,
            "idle": 2,
            "multiplier": 3,
            "click": 4,
            "owner": api_host + f"user/{self.user1.username}/",
            "sprite": "sprite.com"
        })

        # Check if created successfully
        self.assertEqual(response.status_code, 201)

        # Check if that new item was added
        response = self.client.get(api_host + "item/", params={
            "format": "json"
        })
        items = response.json()
        items = list(filter(lambda i: i["name"] == "new_item", items))
        self.assertTrue(len(items) == 1)

    def test_get_item(self):
        """
        Test getting a single item
        """
        # Gets item
        response = self.client.get(api_host + f"item/{self.item1.id}", params={
            "format": "json"
        })

        # Assert that the request was valid
        self.assertEqual(response.status_code, 200)

        # Check if that's really the item we wanted
        self.assertEqual(response.json()["name"], self.item1.name)

    def test_update_item(self):
        """
        Test updating a whole item
        """
        # Prepares a new item
        new_item = {
            "id": self.item1.id,
            "name": "new_name",
            "description": "desc",
            "type": "H",
            "rarity": "2",
            "idle": 3.0,
            "multiplier": 4.0,
            "click": 5.0,
            "owner": api_host + f"user/{self.user1.username}/",
            "sprite": "newsprite.com"
        }

        # Updates item
        response = self.client.put(api_host + f"item/{self.item1.id}/", json=new_item)

        # Assert that the request was valid
        self.assertEqual(response.status_code, 200)

        # Check if the updates happened
        response = self.client.get(api_host + f"item/{self.item1.id}/", params={
            "format": "json"
        })
        item = response.json()
        self.assertEqual(item["name"], new_item["name"])
        self.assertEqual(item["description"], new_item["description"])
        self.assertEqual(item["type"], new_item["type"])
        self.assertEqual(item["rarity"], new_item["rarity"])
        self.assertEqual(item["idle"], new_item["idle"])
        self.assertEqual(item["multiplier"], new_item["multiplier"])
        self.assertEqual(item["click"], new_item["click"])
        self.assertTrue(self.user1.username in item["owner"])
        self.assertEqual(item["sprite"], new_item["sprite"])

    def test_patch_item(self):
        """
        Test patching an item
        """
        response = self.client.patch(api_host + f"item/{self.item1.id}/", json={
            "idle": 56.0
        })

        # Assert that the request was valid
        self.assertEqual(response.status_code, 200)

        # Check if the value changed
        response = self.client.get(api_host + f"item/{self.item1.id}/", params={
            "format": "json"
        })
        item = response.json()
        self.assertEqual(item["idle"], 56.0)

    def test_delete_item(self):
        """
        Test deleting an item
        """
        response = self.client.delete(api_host + f"item/{self.item1.id}/")

        # Check if the item's still there
        response = self.client.get(api_host + f"item/", params={
            "format": "json"
        })
        items = response.json()
        items = list(map(lambda i: i["name"], items))
        self.assertNotIn(self.item1.name, items)

        # Check if the equipped item is now null
        response = self.client.get(api_host + f"user/{self.user1.username}/", params={
            "format": "json"
        })
        user = response.json()
        self.assertIsNone(user["equipped_items_hat"])
