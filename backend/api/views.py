from django.views.generic import TemplateView
from django.views.decorators.cache import never_cache
from django.shortcuts import get_object_or_404
from django.http import Http404
from rest_framework import viewsets, generics, renderers, mixins
from rest_framework.decorators import action, api_view, permission_classes
from rest_framework.response import Response
from rest_framework.views import APIView
from django.utils.timezone import make_aware
from rest_framework.reverse import reverse

from .permissions import IsGetOrIsAuthenticatedOwner, UserViewSetPermission, InitialisePermission, ItemViewSetPermission, \
                        IsAuthenticatedOwner, ShopPermission
from .models import Message, User, Item, Followers, Email
from .serializers import MessageSerializer, UserSerialiser, ItemSerialiser, FollowersSerializer, EmailSerializer
from django.utils.timezone import make_aware

from datetime import datetime
import time, random, math

# Serve Vue Application
index_view = never_cache(TemplateView.as_view(template_name='index.html'))

class MessageViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows messages to be viewed or edited.
    """
    queryset = Message.objects.all()
    serializer_class = MessageSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerialiser
    permission_classes = [UserViewSetPermission]
    lookup_field = 'username'

    def update(self, request, *args, **kwargs):
        data = request.data

        partial = kwargs.pop('partial', False)
        ap_gain = data.pop('ap_gain', 0)
        is_Logout = data.pop('logout', False)

        # Gets and serialises the user 
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=data, partial=partial)

        # Validates user data
        serializer.is_valid(raise_exception=True)

        # Increments user's lifetime_ap
        serializer.validated_data["lifetime_ap"] = instance.lifetime_ap + ap_gain

        # Save the datetime of logging out
        if is_Logout:
            serializer.validated_data["last_login"] = make_aware(datetime.now())

        self.perform_update(serializer)
        
        return Response(serializer.data)

    """
    @action(detail=True, renderer_classes=[renderers.JSONRenderer], name="Get Inventory")
    def inventory(self, request, *args, **kwargs):
        user = self.get_object()
        inventory_list = list(user.inventory())
        inventory_list = map(lambda i: ItemSerialiser(i, context={'request': request}).data, inventory_list)
        print(list(inventory_list))
        print("args:")
        print(list(args))
        print("kwargs:")
        print(list(kwargs))
        return Response(inventory_list)
    """

@api_view(['GET'])
@permission_classes([InitialisePermission])
def initialise_user_data(request, user_username, format=None):

    # Gets the user
    user = get_object_or_404(User, username=user_username)

    # Gets and seriliases the user's equipped items
    serialised_equipped_hat = ItemSerialiser(user.equipped_items_hat, context={"request": request}).data
    serialised_equipped_face = ItemSerialiser(user.equipped_items_face, context={"request": request}).data
    serialised_equipped_toy = ItemSerialiser(user.equipped_items_toy, context={"request": request}).data

    # Check if the user is a new user
    if user.last_login:

        # Time taken for hunger to get to 0 from 1 - 30 minutes
        t = 30 * 60

        # Rate of hunger decreases per second
        decrease_rate = 1 / t

        # Gets the datetime of last_login and login (which should be NOW) and converts it to unix time 
        last_login_unix_time = int(time.mktime(user.last_login.timetuple()))
        login_unix_time = int(time.mktime(datetime.now().timetuple()))

        # Time interval of being offline
        offline_interval = login_unix_time - last_login_unix_time

        # Add all the equipped items' multiplier together
        multiplier = sum([item["multiplier"] for item in [serialised_equipped_hat, serialised_equipped_face, serialised_equipped_toy]])

        # Add all the equipped items' idle together
        idle_tick = sum([item["idle"] for item in [serialised_equipped_hat, serialised_equipped_face, serialised_equipped_toy]])

        # Initialises variables for later assignments
        hunger = user.hunger
        ap = user.ap
        ap_gain = 0

        # Calculates the amount AP has been gained while the user is offline
        while offline_interval > 0:
            if hunger > 0.66 or hunger < 0.33:
                ap_gain += idle_tick * multiplier * 0.5
            else:
                ap_gain += idle_tick * multiplier * 2

            offline_interval -= 1
            hunger -= decrease_rate if hunger - decrease_rate > 0 else 0
        
        # Updates the user's record in the database
        user.ap += ap_gain
        user.hunger = hunger
        user.lifetime_ap += ap_gain
        user.save()

    res = {
        "ap": user.ap,
        "hunger": user.hunger,
        "equipped_items_hat": serialised_equipped_hat,
        "equipped_items_face": serialised_equipped_face,
        "equipped_items_toy": serialised_equipped_toy,
    }

    return Response(res, status=200)

class ItemViewSet(viewsets.ModelViewSet):
    queryset = Item.objects.all()
    serializer_class = ItemSerialiser
    permission_classes = [ItemViewSetPermission]

@api_view(['POST'])
@permission_classes([ShopPermission])
def generate_new_item(request, format=None):
    data = request.data
    error_res = {}

    # Check if the request provides all the necessary fields
    if not data.get("username", None):
        error_res["username"] = ["This field is required."]

    if not data.get("rarity", None):
        error_res["rarity"] = ["This field is required."]

    if error_res:   
        return Response(error_res, status=400)

    username, rarity = data["username"], data["rarity"]

    # Gets user if it exists
    user = get_object_or_404(User, username=username)

    # Check if the lifetime_ap of the user is more than 1 since it'll mess up the calculation later on if it's somehow zero.
    if user.lifetime_ap < 1:
        return Response({
            "detail": "AP less than one, How?"
        },status=400)

    # Map for a variance of sprite based on the item type
    spritesMap = {
        "H": ['Chef', 'Tophat', 'Beanie'],
        "F": ['Sunglasses', 'Moustache', 'Monocle'],
        "T": ['Ball', 'Teddy', 'Bone'],
    }

    # Gets a randomised item type if none is specified
    # The type of item is only be specified for testing purposes
    if not data.get("type", None):
        types = ["H", "F", "T"]
        random.shuffle(types)
        randomised_type = types[0]
    else:
        randomised_type = data["type"]

    # Gets a randomised sprite
    sprites = spritesMap[randomised_type]
    random.shuffle(sprites)
    randomised_sprite = sprites[0]


    # Map for getting a randomised idle value based on rarity
    rarityMapIdle = {
        "Common": random.randrange(math.floor(user.lifetime_ap * 0.1 / 100), math.ceil(user.lifetime_ap * 1 / 100), 1)+1,
        "Rare": random.randrange(math.floor(user.lifetime_ap * 0.2 / 100), math.ceil(user.lifetime_ap * 1.2 / 100), 1)+1,
        "Epic": random.randrange(math.floor(user.lifetime_ap * 0.6 / 100), math.ceil(user.lifetime_ap * 1.6 / 100), 1)+1,
    }

    # Map for getting a randomised click value based on rarity
    rarityMapClick = {
        "Common": random.randrange(math.floor(user.lifetime_ap * 0.1 / 100), math.ceil(user.lifetime_ap * 1 / 100), 1)+1,
        "Rare": random.randrange(math.floor(user.lifetime_ap * 0.4 / 100), math.ceil(user.lifetime_ap * 1.4 / 100), 1)+1,
        "Epic": random.randrange(math.floor(user.lifetime_ap * 0.8 / 100), math.ceil(user.lifetime_ap * 1.8 / 100), 1)+1,
    }

    # Map for getting a randomised multiplier value based on rarity
    rarityMapMulti = {
        "Common": max(math.ceil((random.randrange(100, 200)/100) * math.log(user.lifetime_ap, 64)),1),
        "Rare": max(math.ceil((random.randrange(130, 230)/100) * math.log(user.lifetime_ap, 64)),1),
        "Epic": max(math.ceil((random.randrange(160, 260)/100) * math.log(user.lifetime_ap, 64)),1),
    }

    idleValue = 0 #toys
    clickValue = 0 #hat
    multiValue = 0 #face

    # Computes the idle AP, click AP, and multiplier based on item and rarity.
    """
    Attributes are prioritied differently for each type of the items, and users are able to get 
    more attributes by getting higher rarity items.

    Hat:
        Common -> Click
        Rare -> Click, idle
        Epic -> Click, idle, multiplier
    
    Toy:
        Common -> Idle
        Rare -> Idle, click 
        Epic -> Idle, click, multiplier

    Face:
        Common -> Multiplier
        Rare -> Multiplier, click
        Epic -> Multiplier, click, idle

    """
    if randomised_type == "H":
        clickValue = rarityMapClick[rarity]

        if rarity != "Common":
            idleValue = rarityMapIdle[rarity]

        if rarity == "Epic":
            multiValue = rarityMapMulti["Epic"]

    elif randomised_type == "T":
        idleValue = rarityMapIdle[rarity]

        if rarity != "Common":
            clickValue = rarityMapClick[rarity]

        if rarity == "Epic":
            multiValue = rarityMapMulti["Epic"]

    else:
        multiValue = rarityMapMulti[rarity]

        if rarity != "Common":
            clickValue = rarityMapClick[rarity]

        if rarity == "Epic":
            idleValue = rarityMapIdle["Epic"]

    # Create a new item
    new_item = Item.objects.create(
        name= f"{rarity} {randomised_sprite}", 
        description= f"A new {randomised_sprite}",
        type=randomised_type, 
        rarity=rarity, 
        idle=idleValue,
        multiplier=multiValue,
        click=clickValue,
        owner=user, 
        sprite=randomised_sprite
    )

    return Response(ItemSerialiser(new_item, context={"request": request}).data, status=200)

class InventoryList(APIView):
    permission_classes = [IsAuthenticatedOwner]

    def get(self, request, user_username, format=None):
        """
        Gets the inventory of a user
        """
        # Gets the user
        user = User.objects.get(username=user_username)

        # Gets the inventory
        inventory = list(user.inventory())

        # Serialises the inventory
        serialised_inventory = map(lambda i: ItemSerialiser(i, context={'request': request}).data, inventory)
        serialised_inventory = list(serialised_inventory)

        # Responds with serialised inventory
        return Response(serialised_inventory)

    def put(self, request, user_username, format=None):
        """
        Adds an item to the user's inventory
        """

        # Check if 'id' field is received
        if not request.data.get("id", None):
            return Response({
                "id": ["This field is required."]
            }, status=400)

        # Gets the user and item
        user = User.objects.get(username=user_username)
        item = Item.objects.get(id=request.data["id"])

        # Sets the owner of the selected item to this user
        item.owner = user
        item.save()

        # Returns new inventory
        return self.get(request, user_username, format)

    def delete(self, request, user_username, format=None):
        """
        Clears the entire inventory
        """
        # Gets the inventory of the user
        user = User.objects.get(username=user_username)
        inventory = user.inventory()

        # Goes through each item and delete them
        for item in inventory:
            item.delete()

        # Returns new inventory (which should be an empty list)
        return Response(status=204)


class InventoryDetail(APIView):
    permission_classes = [IsAuthenticatedOwner]

    def item_in_inventory(self, user, item_id):
        """
        Checks if item is in a user's inventory
        :param user: User object
        :param item_id: Item object ID
        :return: True if user has item, False if not
        """
        return user.inventory().filter(id=item_id).exists()

    def get(self, request, user_username, item_id, format=None):
        # Gets inventory
        user = User.objects.get(username=user_username)
        inventory = user.inventory()

        # If item is not there, 404
        if not self.item_in_inventory(user, item_id):
            return Response({
                "detail": f"Item doesn't exist in {user_username}'s inventory."
            }, status=404)
        else:
            # Gets and serialises item and returns it
            item = Item.objects.get(id=item_id)
            serialised_item = ItemSerialiser(item, context={"request": request}).data
            return Response(serialised_item)

    def delete(self, request, user_username, item_id, format=None):
        # Gets inventory
        user = User.objects.get(username=user_username)
        inventory = user.inventory()

        # If item is not there, 404
        if not self.item_in_inventory(user, item_id):
            return Response({
                "detail": f"Item doesn't exist in {user_username}'s inventory."
            }, status=404)
        else:
            # Gets and deletes item
            item = Item.objects.get(id=item_id)
            item.delete()
            return Response(status=204)


@api_view(['GET'])
@permission_classes([IsAuthenticatedOwner])
def inventory_equip(request, user_username, item_id, format=None):
    # Gets user
    user = User.objects.get(username=user_username)

    # If we don't have the item, return error
    if not user.inventory().filter(id=item_id).exists():
        return Response({
            "detail": f"Item doesn't exist in {user_username}'s inventory."
        }, status=404)
    else:
        # Gets item and item type
        item = Item.objects.get(id=item_id)
        item_type = item.type

        # For each item type, equip at a certain slot
        if item_type == "H":
            user.equipped_items_hat = item
        elif item_type == "F":
            user.equipped_items_face = item
        elif item_type == "T":
            user.equipped_items_toy = item

        # Save user
        user.save()

        # Return new user
        return Response(
            UserSerialiser(
                User.objects.get(username=user_username),
                context={
                    "request": request
                }
            ).data
        )


@api_view(['GET'])
@permission_classes([IsAuthenticatedOwner])
def inventory_unequip(request, user_username, item_id, format=None):
    # Gets user
    user = User.objects.get(username=user_username)

    # If we don't have the item, return error
    if not user.inventory().filter(id=item_id).exists():
        return Response({
            "detail": f"Item doesn't exist in {user_username}'s inventory."
        }, status=404)
    else:
        # Gets item and item type
        item = Item.objects.get(id=item_id)
        item_type = item.type

        # Check if equipped. If not, error
        if user.equipped_items_hat != item and \
                user.equipped_items_face != item and \
                user.equipped_items_toy != item:
            return Response({
                "detail": f"Item is not currently equipped."
            }, status=409)

        # For each item type, nullify certain slot
        if item_type == "H":
            user.equipped_items_hat = None
        elif item_type == "F":
            user.equipped_items_face = None
        elif item_type == "T":
            user.equipped_items_toy = None

        # Save user
        user.save()

        # Return new user
        return Response(
            UserSerialiser(
                User.objects.get(username=user_username),
                context={
                    "request": request
                }
            ).data
        )

class FollowingList(APIView):
    permission_classes = [IsGetOrIsAuthenticatedOwner]

    def get(self, request, user_username, format=None):
        # Gets user
        user = get_object_or_404(User, username=user_username)

        # Gets a list of following user
        followings = list(Followers.objects.filter(follower=user))

        # Serialises the list of following user
        serialised_followings = {"followings" : [FollowersSerializer(i).data["following"] for i in followings]}

        # Responds with serialised list of following user
        return Response(serialised_followings, status=200)

    def post(self, request, user_username, format=None):
        # Gets request body
        data = request.data

        # Check if 'following' field is received
        if not data.get("following", None):
            return Response({
                "following": ["This field is required."]
            }, status=400)

        # Check if the user tries to follow himself/herself
        if user_username == data["following"]:
            return Response({
                "detail": "Users cannot follow themselves."
            }, status=400) 

        # Gets user and the specified user for following
        user = get_object_or_404(User, username=user_username)
        follow_user = get_object_or_404(User, username=data["following"])

        # Check if the user is already following the specified user
        record = Followers.objects.filter(follower=user, following=unfollow_user)
        if record.exists():
            return Response({
                "detail": "The user is already following the specified user."
            }, status=400)
        
        # Create a one-direction relation with serialiser
        data["follower"] = user_username
        serialiser = FollowersSerializer(data=data)
        serialiser.is_valid(raise_exception=True)
        serialiser.save()
        return Response(status=201)

    def delete(self, request, user_username, format=None):
        # Gets user
        user = get_object_or_404(User, username=user_username)
        followings = Followers.objects.filter(follower=user)
        
        # Unfollows everyone
        followings.delete()
        return Response(status=204)

@api_view(['DELETE'])
@permission_classes([IsGetOrIsAuthenticatedOwner])
def unfollow_user(request, user_username, unfollow_user, format=None):
    # Gets user and the specified user for unfollowing
    user = get_object_or_404(User, username=user_username)
    unfollow_user = get_object_or_404(User, username=unfollow_user)

    # Check if the user is following the specified user
    record = Followers.objects.filter(follower=user, following=unfollow_user)
    if not record.exists():
        return Response({
                "detail": "The user is not following the specified user."
            }, status=400)
    
    # Remove the following from user
    record.delete()
    return Response(status=204)

@api_view(['GET'])
@permission_classes([IsGetOrIsAuthenticatedOwner])
def followers_list(request, user_username, format=None):
    # Gets user
    user = get_object_or_404(User, username=user_username)

    # Gets a list of followers
    followers = list(Followers.objects.filter(following=user))

    # Serialised the list of followers
    serialised_followers = {"followers" : [FollowersSerializer(i).data["follower"] for i in followers]}

    # Responds with serialised list of followers
    return Response(serialised_followers, status=200)

class EmailList(APIView):
    permission_classes = [IsAuthenticatedOwner]

    def get(self, request, user_username, format=None):

        # Gets emails of the user
        emails = Email.objects.filter(recipent=user_username)
        serialized_emails = EmailSerializer(emails, many=True)
        return Response(serialized_emails.data, status=200)
    
    def post(self, request, user_username, format=None):
        data = request.data
        error_res = {}
        required_fields = ["recipent", "subject", "content"]

        # Check if the request provides all the necessary fields
        for f in required_fields:
            if not data.get(f, None):
                error_res[f] = "This field is required."

        if error_res:   
            return Response(error_res, status=400)

        recipent, subject, content = data["recipent"], data["subject"], data["content"]

        # Check if the content is empty
        if not content.strip():
            return Response({
                "detail": "Content must not be empty"
            }, status=400)
        
        # Check if the subject is empty
        if not subject.strip():
            return Response({
                "detail": "Subject must not be empty"
            }, status=400)

        # Check if the user is trying to send email ot themselves
        if recipent == user_username:
            return Response({
                "detail": "Users cannot send themselves"
            }, status=400)

        sender_user = get_object_or_404(User, username=user_username)
        recipent_user = get_object_or_404(User, username=recipent)
        
        email = Email.objects.create(sender=sender_user, recipent=recipent_user, subject=subject,content=content)
        return Response(status=200)
    
    def delete(self, request, user_username, format=None):
        data = request.data

        # Check if the request provides all the necessary fields
        if not data.get("id", None):
            return Response({
                "id": "This field is required."
            }, status=400)

        id = data["id"]

        # Gets the email which has the given id
        email = Email.objects.get(pk=id)

        # Check if the user is trying to remove others' email
        if str(email.recipent) != user_username:
            return Response({
                "detail": "Users cannot delete others' emails"
            }, status=400)
        
        email.delete()
        return Response(status=204)



