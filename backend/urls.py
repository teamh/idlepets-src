"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
"""

from django.contrib import admin
from django.urls import path, include
from rest_framework_nested import routers
from rest_framework_simplejwt import views as jwt_views

from .api.views import index_view, MessageViewSet, UserViewSet, ItemViewSet, InventoryList, InventoryDetail,\
    inventory_equip, inventory_unequip, initialise_user_data, generate_new_item,FollowingList, unfollow_user, followers_list,\
    EmailList

router = routers.DefaultRouter()
router.register('messages', MessageViewSet)
router.register('user', UserViewSet)
router.register('item', ItemViewSet)

urlpatterns = [

    path('admin/', admin.site.urls),

    # http://localhost:8000/
    path('', index_view, name='index'),

    # http://localhost:8000/api/<router-viewsets>
    path('api/', include(router.urls)),
    #path('api/', include(inventory_router.urls)),
    path('api/user/<str:user_username>/inventory/', InventoryList.as_view()),
    path('api/user/<str:user_username>/inventory/<int:item_id>/', InventoryDetail.as_view()),
    path('api/user/<str:user_username>/inventory/<int:item_id>/equip', inventory_equip),
    path('api/user/<str:user_username>/inventory/<int:item_id>/unequip', inventory_unequip),

    path('api/user/<str:user_username>/initialise/', initialise_user_data),

    path('api/user/<str:user_username>/following/', FollowingList.as_view()),
    path('api/user/<str:user_username>/following/<str:unfollow_user>/', unfollow_user),
    path('api/user/<str:user_username>/followers/', followers_list),

    path('api/shop/', generate_new_item),
    path('api/user/<str:user_username>/email/', EmailList.as_view()),

    # http://localhost:8000/api/token/<action>
    path('api/token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('api/token/verify/', jwt_views.TokenVerifyView.as_view(), name="token_verify"),

    # http://localhost:8000/api/admin/
    path('api/admin/', admin.site.urls)
]


