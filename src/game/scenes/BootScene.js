import { Scene } from 'phaser';

import sky from "@/game/assets/sky.png";
import PetHappy from "@/assets/PetSprites/PetHappy.png";
import HatChef from "@/assets/PetSprites/Chef.png";
import AccessoryMoustache from "@/assets/PetSprites/Moustache.png";

import Pipe from "@/game/assets/pipe.png";
import GameOver from "@/game/assets/gameover.png";
import Grass from "@/game/assets/grass.png";

import Start from "@/game/assets/start.png";


export default class BootScene extends Scene {
    constructor () {
        super({ key: 'BootScene' });
    }

    create () {
        let textureCount = 0; // Texture count so far
        const noOfTextures = 8; // Number of textures

        // On texture load, if all textures are loaded, go to next scene
        this.textures.on('onload', () => {
            if (++textureCount >= noOfTextures)
                this.scene.start('PlayScene');
        });

        // Loads all textures
        this.textures.addBase64('sky', sky);

        this.textures.addBase64('Pet', PetHappy);
        this.textures.addBase64('Hat', HatChef);
        this.textures.addBase64('Accessory', AccessoryMoustache);
        this.textures.addBase64('Pipe', Pipe);
        this.textures.addBase64('Grass', Grass);

        this.textures.addBase64('GameOver', GameOver);
        this.textures.addBase64('Start', Start);
    }
}
