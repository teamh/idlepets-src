import { Scene } from 'phaser';
import Phaser from 'phaser';
import store from '../../store';
import Axios from 'axios'

export default class PlayScene extends Scene {

    /** How much force the pet can jump */
    petHopForce = 250;

    /** A list of Phaser.Sprite pipes */
    pipes = [];

    /** Speed of pipes */
    pipeSpeed = 200;

    /** True if it's game over, false if it's not */
    isGameOver = false;

    /** Game over sprite */
    gameOverSprite = null;

    /** Start sprite */
    startSprite = null;

    /** Pipe time event */
    pipeTimeEvent = null;

    /** Score */
    score = 0;

    /** Affinity earned */
    affinityEarned = 0;

    /** Local variable for Affection at the start of the game */
    ap = store.getters.currentAffection;

    /** Speed of grass */
    grassSpeed = 2;

    constructor() {
        super({ key: 'PlayScene' });
    }

    create() {

        // BG image
        this.add.image(400, 300, 'sky');

        // Start sprite
        this.startSprite = this.add.image(400, 300, 'Start');
        this.startSprite.depth = 100;

        // Grass
        this.grass = this.add.tileSprite(400, 580, 800, 45, 'Grass');

        // Adds score text
        this.textScoreObj = this.add.text(10, 10, "Score: 0");
        this.textScoreObj.depth = 100;
        this.textAffinityObj = this.add.text(10, 30, "Affection earned: 0");
        this.textAffinityObj.depth = 100;

        // Creates pet
        this.petContainer = this.add.container(100, 200, [
            this.make.sprite({
                x: 0,
                y: 0,
                key: 'Pet'
            }),
            this.make.sprite({
                x: 0,
                y: 0,
                key: 'Hat'
            }),
            this.make.sprite({
                x: 0,
                y: 0,
                key: 'Accessory'
            })
        ]);
        this.petContainer.depth = 50;

        // Sets size of pet (so that it's not massive)
        this.petContainer.setScale(0.1, 0.1);
        this.petContainer.setSize(32, 32);

        // Sets physics of pet
        this.physics.world.enable(this.petContainer);
        this.petContainer.body.gravity = new Phaser.Math.Vector2(0, 300);

        // When user taps, pet glides up
        this.input.on('pointerdown', pointer => {
            // If game over OR if we haven't started the game yet, reset, else move pet
            if (this.isGameOver || this.startSprite !== null) {
                this.isGameOver = false;
                this.startgame();

                // If we haven't started the game
                if (this.startSprite !== null) {
                    this.startSprite.destroy();
                    this.startSprite = null;
                }
            } else {
                // Hops the pet up
                this.petContainer.body.velocity = new Phaser.Math.Vector2(0, -this.petHopForce);
            }
        });

        this.petContainer.body.setCollideWorldBounds(true);
        this.petContainer.body.onWorldBounds = true;
        this.physics.world.on('worldbounds', () => {
            // If player is NOT touching the top
            if (this.petContainer.body.y !== 0)
                this.gameover();
        });

        // Creates pipes in a repeated timed event
        this.pipeTimeEvent = this.time.addEvent({
            delay: 1500,
            callback: () => {
                // Randomises pipe offset
                const pipeOffset = Math.random() * 350 - 100;

                // Makes pipe children
                const topPipe = this.make.sprite({
                    x: 0,
                    y: 600 + pipeOffset,
                    key: 'Pipe'
                });
                const bottomPipe = this.make.sprite({
                    x: 0,
                    y: -150 + pipeOffset,
                    flipY: true,
                    key: 'Pipe'
                });
                const middle = this.make.sprite({
                    x: 0,
                    y: 230 + pipeOffset,
                    flipY: true,
                    key: ''
                });

                // Set physics with pipe children
                for (const pipeChild of [topPipe, bottomPipe]) {
                    this.physics.world.enable(pipeChild);
                    pipeChild.body.allowGravity = false;
                    pipeChild.body.immovable = true;
                    pipeChild.body.setSize(32, pipeChild.body.height);
                    this.physics.add.collider(this.petContainer, pipeChild, (pet, pipe) => {
                        this.gameover();
                    });
                }

                // Set middle properties
                this.physics.world.enable(middle);
                middle.body.allowGravity = false;
                middle.body.immovable = true;
                middle.body.setSize(32, 128);
                middle.alpha = 0;
                this.physics.add.collider(this.petContainer, middle, (pet, middle) => {
                    // Destroys middle object
                    middle.destroy();

                    // Increments score
                    this.score++;
                    this.textScoreObj.text = `Score: ${this.score}`;

                    // Updates affinity
                    let addedAffinity = Math.ceil(store.getters.affectionPerClick * store.getters.multiplier * 10);
                    addedAffinity = addedAffinity === 0 ? 1 : addedAffinity; // If we have no affinity, give player 1
                    this.affinityEarned += addedAffinity;
                    this.textAffinityObj.text = `Affection earned: ${this.affinityEarned}`;
                });

                // Creates pipe container
                const pipe = this.add.container(800, 0, [topPipe, bottomPipe, middle]);

                // Sets pipe container physics (just move left)
                this.physics.world.enable(pipe);
                pipe.body.allowGravity = false;
                pipe.body.velocity = new Phaser.Math.Vector2(-this.pipeSpeed, 0);

                // Adds pipe container to array
                this.pipes.push(pipe);
            },
            loop: true
        });

        // Stops game for now
        this.stopgame();

        // DEBUG
        this.input.keyboard.on('keyup-' + 'Q', e => this.stopgame());
        this.input.keyboard.on('keyup-' + 'W', e => this.startgame());
    }

    update() {
        // Makes grass move
        this.grass.tilePositionX += this.grassSpeed;

        // Goes through each pipe
        for (const pipe of this.pipes)
        {
            // If this pipe has gone too far
            if (pipe.body.x < -32) {
                // Destroy pipe
                pipe.destroy();

                // Remove from array
                this.pipes = this.pipes.filter(p => p !== pipe);
            }
        }
    }

    /**
     * Stops the game, usually used for game overs.
     * Don't run this when instigating a game over; use the "gameover" method instead
     */
    stopgame() {
        // Stop all pipes
        for (const pipe of this.pipes) {
            pipe.body.velocity = new Phaser.Math.Vector2(0, 0);
        }

        // Stop grass
        this.grassSpeed = 0;

        // Stop pet from moving
        this.petContainer.body.allowGravity = false;
        this.petContainer.body.velocity = new Phaser.Math.Vector2(0, 0);

        // Pauses pipe time event
        this.pipeTimeEvent.paused = true;
    }

    /**
     * Starts a new game, typically resetting everything
     */
    startgame() {
        // Resume pipe time event
        this.pipeTimeEvent.paused = false;

        // Make pet move again
        this.petContainer.body.allowGravity = true;
        this.petContainer.body.velocity = new Phaser.Math.Vector2(0, -this.petHopForce);
        this.petContainer.y = 200;

        // Destroy all pipes
        this.pipes.forEach(p => p.destroy());
        this.pipes = [];

        // Reset score
        this.score = 0;
        this.affinityEarned = 0;
        this.textScoreObj.text = "Score: 0";
        this.textAffinityObj.text = "Affection earned: 0";

        // Sets grass speed
        this.grassSpeed = 2;

        // Destroy game over sprite (if it exists)
        if (this.gameOverSprite !== null) {
            this.gameOverSprite.destroy();
            this.gameOverSprite = null;
        }
    }

    /**
     * Instigates a game over
     */
    gameover() {
        // If we haven't game overed yet
        if (!this.isGameOver) {
            // Set flag
            this.isGameOver = true;

            // Stops the game
            this.stopgame();

            // Creates game over sprite
            this.gameOverSprite = this.add.sprite(400, 300, "GameOver");

            store.commit('incrementAffection', this.affinityEarned);

            Axios({
                url: window.location.protocol + '//' + window.location.hostname + ':' + window.location.port + '/api/user/' + localStorage.getItem('username') + '/',
                data: {
                  "ap": store.getters.currentAffection,
                  "hunger": store.getters.foodLevel / 100,
                  "ap_gain": store.getters.ap_gain,
                  "logout": 1,
                  },
                method: 'PATCH'
              }).then(resp => {
                //patch successful, therefore reset ap_gain
                store.commit("resetApGain");
                //updateBoxPrices based on lifetime ap
                let realAffectionPerSecond = store.getters.affectionPerSecond * store.getters.multiplier;
                let realClickAffection = store.getters.affectionPerClick * store.getters.multiplier;
                store.commit("setEpicBoxPrice", Math.max(Math.ceil(realAffectionPerSecond * 100),Math.ceil(realClickAffection *500)));
                store.commit("setRareBoxPrice",  Math.max(Math.ceil(realAffectionPerSecond * 30),Math.ceil(realClickAffection *200)));
                store.commit("setCommonBoxPrice", Math.max(Math.ceil(realAffectionPerSecond * 10),Math.ceil(realClickAffection *50)));

              }).catch(err => {
                console.log(err);
              })
            
        }
    }
}
