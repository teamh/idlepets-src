/* eslint-disable */

import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '@/views/Login'
import Home from '@/views/Home'
import Axios from 'axios'
import store from '@/store/index.js'

Vue.use(VueRouter);

const routes = [{
  path: '/',
  name: 'home',
  component: Home,
  meta: {
    requiresAuth: true
  }
}, {
  path: '/login',
  name: 'login',
  component: Login
}, {
  path: '/minigame',
  name: 'minigame',
  component: () => import("./views/Minigame.vue"),
  meta: {
    requiresAuth: true
  }
}, ];

const router = new VueRouter({
  bases: process.env.BASE_URL,
  mode: 'history',
  routes
});

function checkAccess(access) {
  return new Promise((resolve, reject) => {
    Axios({
      url: window.location.protocol + '//' + window.location.hostname + ':' + window.location.port + '/api/token/verify/',
      data: {
        "token": access
      },
      method: 'POST'
    }).then(resp => {
      resolve(resp)
    }).catch(err => {
      reject(err.response)
    })
  })
}

function checkRefresh(refresh) {
  delete Axios.defaults.headers.common['Authorization']
  return new Promise((resolve, reject) => {
    Axios({
      url: window.location.protocol + '//' + window.location.hostname + ':' + window.location.port + '/api/token/verify/',
      data: {
        "refresh": refresh
      },
      method: 'POST'
    }).then(resp => {
      resolve(resp)
    }).catch(err => {
      reject(err)
    })
  })
}

function isLoggedIn() {
  return localStorage.getItem('access') && localStorage.getItem('refresh')
}


router.beforeEach((to, from, next) => {

  var access = localStorage.getItem('access')
  var refresh = localStorage.getItem('refresh')

  if ((to.name === "login") && isLoggedIn()) {
    next("/")
    return
  }

  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (isLoggedIn()) {
      checkAccess(access).then(resp => {
        if(to.name === "minigame" && store.getters.currentAffection === 0){
          next('/')
        }
        next()
        return
      }).catch(err => {
        console.log(err)
        console.log("Access token invalid")
        //access token is invalid
        if (err.status === 401) {
          //check if refresh still valid
          checkRefresh(refresh).then(resp => {
            //refresh is valid
            localStorage.setItem('access', resp.data.access)
            Axios.defaults.headers.common['Authorization'] = resp.data.access
            next('/')
            return
          }).catch(err => {
            //refresh is not valid
            console.log("Refresh is not valid")
            store.dispatch('forceLogout')
            next('/login')
            return
          })  
        }
        
        console.log("An unexpected error has occured.")
        next('/login')
        return

      })
      return
    }
    next('/login')
  } else {
    next()
  }
})

export default router