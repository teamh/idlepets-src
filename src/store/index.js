/* eslint-disable */

import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios'
import router from '@/router'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    token: localStorage.getItem('token') || '',
    user: {},
    username: '',
    foodLevel: 30,
    currentAffection: 0,
    ap_gain: 0,
    affectionPerSecond: 0,
    affectionPerClick: 0,
    multiplier: 2,
    equippedHat: null,
    equippedFace: null,
    equippedToy: null,
    intervalID: null,
    saveTimer: 0,
    settingsEnabled: false,
    shopEnabled: false,
    inventoryEnabled: false,
    mailEnabled: false,
    epicBoxPrice: 1,
    rareBoxPrice: 1,
    commonBoxPrice: 1,
    messages: [],
    newMessages: false,
    unwatchFuntion: false
  },
  getters: {
    newMessages: state => state.newMessages,
    inventoryEnabled: state => state.inventoryEnabled,
    settingsEnabled: state => state.settingsEnabled,
    shopEnabled: state => state.shopEnabled,
    mailEnabled: state => state.mailEnabled,
    saveTimer: state => state.saveTimer,
    intervalID: state => state.intervalID,
    username: state => state.username,
    foodLevel: state => state.foodLevel,
    currentAffection: state => state.currentAffection,
    ap_gain: state => state.ap_gain,
    affectionPerSecond: state => {
        return state.equippedHat.idle + state.equippedFace.idle + state.equippedToy.idle;
    },
    affectionPerClick: state => {
        return state.equippedHat.click + state.equippedFace.click + state.equippedToy.click;
    },
    multiplier: state => {
        return state.equippedHat.multiplier + state.equippedFace.multiplier + state.equippedToy.multiplier;
    },
    foodMultiplier: state => {
      if (state.foodLevel < 100 / 3 || state.foodLevel > 100 * (2 / 3)) {
        return 0.5;
      } else {
        return 2;
      }
    },
    equippedFace: state => state.equippedFace,
    equippedHat: state => state.equippedHat,
    equippedToy: state => state.equippedToy,
    epicBoxPrice: state => state.epicBoxPrice,
    rareBoxPrice: state => state.rareBoxPrice,
    commonBoxPrice: state => state.commonBoxPrice,
    messages: state => state.messages,
    unwatchFuntion: state => state.unwatchFuntion,
    
  },
  mutations: {
    setMessages(state, payload){
      state.messages = payload;
    },
    setIntervalID(state, payload) {
      state.intervalID = payload;
    },
    loggedIn(state, payload) {
      state.username = payload
    },
    logout(state) {
      state.username = '';
      state.currentAffection = 0;
      state.foodLevel = 50;
      window.clearInterval(state.intervalID);
      state.intervalID = null;
      window.onbeforeunload = () => undefined;
      state.messages = [];
      state.mailEnabled = false;
      state.inventoryEnabled = false;
      state.shopEnabled = false;
      
    },
    feedBy(state, payload) {
      if (state.foodLevel + payload < 100) {
        state.foodLevel += payload;
      } else {
        state.foodLevel = 100;
      }
    },
    starveBy(state, payload) {
      if (state.foodLevel - payload > 0) {
        state.foodLevel -= payload;
      } else {
        state.foodLevel = 0;
      }
    },
    setFoodLevel(state, payload) {
      state.foodLevel = payload;
    },
    setCurrentAffection(state, newAffection) {
      state.currentAffection = Math.ceil(newAffection);
    },
    incrementAffection(state, increment){
      increment = Math.ceil(increment);
      state.currentAffection += increment;
      state.ap_gain += increment;
    },
    decrementAffection(state, increment){
      increment = Math.ceil(increment);
      state.currentAffection -= increment;
    },
    setAffectionPerSecond(state, newAPS) {
      state.affectionPerSecond = newAPS;
    },
    setAffectionPerClick(state, newAPC) {
      state.affectionPerClick = newAPC;
    },
    setMultiplier(state, newMultiplier) {
      state.multiplier = newMultiplier;
    },
    setEquippedHat(state, hat) {
      state.equippedHat = hat;
    },
    setEquippedFace(state, face) {
      state.equippedFace = face;
    },
    setEquippedToy(state, toy) {
      state.equippedToy = toy;
    },
    incSaveTimer(state){
      state.saveTimer += 1;
    },
    toggleSettings(state){
      state.inventoryEnabled = false;
      state.mailEnabled = false;
      state.shopEnabled = false;
      state.settingsEnabled = true;
    },
    toggleShop(state){
      state.settingsEnabled = false;
      state.inventoryEnabled = false;
      state.mailEnabled = false;
      state.shopEnabled = true;
    },
    toggleInventory(state){
      state.settingsEnabled = false;
      state.shopEnabled = false;
      state.mailEnabled = false;
      state.inventoryEnabled = true;
    },
    toggleMail(state){
      state.settingsEnabled = false;
      state.shopEnabled = false;
      state.inventoryEnabled = false;
      state.mailEnabled = true;
    },
    setEpicBoxPrice(state, payload){
      state.epicBoxPrice = payload;
    },
    setRareBoxPrice(state, payload){
      state.rareBoxPrice = payload;
    },
    setCommonBoxPrice(state, payload){
      state.commonBoxPrice = payload;
    },
    setInventory(state, payload){
      state.inventory = payload;
    },
    resetApGain(state){
      state.ap_gain = 0;
    }
  },
  actions: {
    play({commit}, payload){
      var increment = this.getters.affectionPerClick * this.getters.multiplier;
      commit('incrementAffection', increment);
    } ,
    login({
      commit
    }, user) {
      return new Promise((resolve, reject) => {
        console.log("Sending login POST (token)");
        Axios({
            url: window.location.protocol + '//' + window.location.hostname + ':' + window.location.port + '/api/token/',
            data: user,
            method: 'POST'
          }).then(resp => {
            localStorage.setItem('access', resp.data.access);
            localStorage.setItem('refresh', resp.data.refresh);
            localStorage.setItem('username', user.username);
            Axios.defaults.headers.common['Authorization'] = "Bearer " + resp.data.access;
            commit('loggedIn', user.username);
            router.push("/");
            resolve(resp)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    register({}, user) {
      return new Promise((resolve, reject) => {
        Axios({
          url: window.location.protocol + '//' + window.location.hostname + ':' + window.location.port + '/api/user/',
          data: user,
          method: 'POST'
        }).then(resp => {
          console.log('Fine: ' + 'http://' + window.location.hostname + ':' + window.location.port + '/api/user/');
          resolve(resp)
        }).catch(err => {
          reject(err)
        })
      })
    },
    logout({
      commit
    }) {
        return new Promise((resolve, reject) => {
          console.log("Sending logout POST" + JSON.stringify({
            "username": localStorage.getItem('username'),
            "ap": this.getters.currentAffection,
            "ap_gain": this.getters.ap_gain,
            "hunger": this.getters.foodLevel / 100,
            "logout": 1,
          }));
          Axios({
            url: window.location.protocol + '//' + window.location.hostname + ':' + window.location.port + '/api/user/' + localStorage.getItem('username') + '/',
            data: {
              "ap": this.getters.currentAffection,
              "ap_gain": this.getters.ap_gain,
              "hunger": this.getters.foodLevel / 100,
              "logout": 1,
            },
            method: 'PATCH'
          }).then(resp => {
            commit('logout');
            this.getters.unwatchFuntion();
            localStorage.removeItem('refresh');
            localStorage.removeItem('access');
            localStorage.removeItem('username');
            delete Axios.defaults.headers.common['Authorization'];
            router.push('/login')
          }).catch(err => {
            console.log(err);
            reject(err.response)
        });
        resolve()
      })
    },
    forceLogout({
      commit
    }) {
      commit('logout');
      localStorage.removeItem('refresh');
      localStorage.removeItem('access');
      localStorage.removeItem('username');
      delete Axios.defaults.headers.common['Authorization']
    }
  },
  modules: {}
})